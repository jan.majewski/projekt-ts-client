﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Net.Sockets;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Threading;


namespace UDPClientWPF
{
    //ENUMy z kodami operacji i odpowiedzi
    public enum OperationType { GetSession = 1, Invite = 2, InviteResponse = 3, Message = 4, Disconnect = 5, Ack = 7 };
    enum SessionEnum { Request = 0, GotSession = 1, Offer = 2 };
    enum InviteEnum { Send = 0, Sent = 1, Error = 2 };
    enum InviteResponseEnum { Accept = 0, Deny = 1, Accepted = 2, Denied = 3 };
    enum MessageEnum { Send = 0, Sent = 1, Error = 2 };
    enum DisconnectEnum { Request = 0, Success = 1, Disconnected = 2 };

    //klasa zarządzająca połączeniem i transmisją
    public partial class MainWindow : Window
    {
        private int _sessionId;
        private bool _connected;
        private IPEndPoint _serverPoint;
        private bool _waitForAck;
        private List<Task> tasks;
        public MainWindow()
        {
            InitializeComponent();

            _connected = false;
            _sessionId = 0;
            _waitForAck = false;
            tasks = new List<Task>();
            Task.Run(async () =>
            {
                using (UdpClient udpReceiver = new UdpClient(1200))
                {
                    //odbieranie pakietów
                    while (true)
                    {
                        if (_serverPoint != null)
                        {
                            var result = await udpReceiver.ReceiveAsync();
                            //MessageBox.Show("a");
                            if (tasks.Count == 0 || tasks.Last().IsCompleted)
                            {
                                tasks.Add(Task.Run(() => DataProcessing(result.Buffer)));
                            }
                            else
                            {
                                tasks.Last().Wait();
                                tasks.Add(Task.Run(() => DataProcessing(result.Buffer)));
                            }
                            //DataProcessing(result.Buffer);
                        }
                    }
                }
            });
        }




        //wykonywanie odpowiednich funkcji na podstawie pola operacji i odpowiedzi
        private async void DataProcessing(byte[] data)
        {
            //MessageBox.Show(_waitForAck.ToString());

            //odczytanie pól operacji i odpowiedzi
            string operation = Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(0, 3);
            string response = Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(3, 3);
            
#if DEBUG
            _ = Application.Current.Dispatcher.BeginInvoke(
                 DispatcherPriority.Background, new Action(() =>
                 {
                     Content.Text += "received: " + Convert.ToString(data[0], 2).PadLeft(8, '0') + " " + _waitForAck + "\n"; }));
#endif

            //operacja 111, ACK
            if (Convert.ToString((int)OperationType.Ack, 2).PadLeft(3, '0') == operation)
            {
                if (GetSessionIdFromData(data) == _sessionId)
                {
                    //zakończenie oczekiwania na ACK, lub rozłączanie w przypadku otrzymania nieoczekiwanego ACK
                    if (_waitForAck)
                    {
                        _waitForAck = false;
                    }
                    else
                    {
                        MessageBox.Show("Krytyczny błąd w transmisji. Nastąpi rozłączenie");
                        Disconnect();
                    }
                }
                else MessageBox.Show("Otrzymano pakiet z innym numerem sesji");
            }
            else
            {
                //jeśli odebrany pakiet nie jest pakietem ACK, wysłanie ACK do serwera
                Send(OperationType.Ack, 0, _sessionId);
            }

            //MessageBox.Show(_waitForAck.ToString() + " post");
            if (!_waitForAck)
            {

                #region odbiór danych
                //operacja 001, otrzymanie id sesji
                if (Convert.ToString((int)OperationType.GetSession, 2).PadLeft(3, '0') == operation)
                {
                    //włączanie i wyłączanie odpowiednich pól w aplikacji
                    _ = Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background, new Action(() =>
                    {
                        ServerIP.IsEnabled = false;
                        ServerPort.IsEnabled = false;
                        ConnectButton.IsEnabled = false;
                        SendInviteButton.IsEnabled = true;
                    }));
                    //przypisywanie id sesji;
                    _sessionId = GetSessionIdFromData(data);
                  
#if DEBUG
                    _ = Application.Current.Dispatcher.BeginInvoke(
                        DispatcherPriority.Background, new Action(() =>
                        {
                            Content.Text += _sessionId.ToString() + "\n";
                        }));
#endif

                    //operacja 001 odpowiedź 001, potwierdzenie uzyskania id

                    Send(OperationType.GetSession, (int)SessionEnum.GotSession, _sessionId);

                    
                }


                // operacja 010, wysłanie zaproszenia
                if (Convert.ToString((int)OperationType.Invite, 2).PadLeft(3, '0') == operation)
                {
                    if (_sessionId == GetSessionIdFromData(data))
                    {
                        // operacja 010 odpowiedź 001, potwierdzenie przekazania zaproszenia
                        if (Convert.ToString((int)InviteEnum.Sent, 2).PadLeft(3, '0') == response)
                        {

                            _ = Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background, new Action(() =>
                            {
                                Content.Text += "Zaproszenie poprawnie wysłane\n";
                            }));

                        }
                        // operacja 010 odpowiedź 000, otrzymano zaproszenie
                        else if (Convert.ToString((int)InviteEnum.Send, 2).PadLeft(3, '0') == response)
                        {
                            //przyjęcie lub odrzucenie zaproszenia
                            if (MessageBox.Show("Otrzymales zaproszenie do chatu", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            {
                                //MessageBox.Show("True");
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Accept, _sessionId);
                                
                            }
                            else
                            {
                                Send(OperationType.InviteResponse, (int)InviteResponseEnum.Deny, _sessionId);
                                
                            }

                        }

                        // operacja 010 odpowiedź 010, brak drugiego klienta
                        else if (Convert.ToString((int)InviteEnum.Error, 2).PadLeft(3, '0') == response)
                        {
                            MessageBox.Show("Nie znaleziono drugiego użytkownika w sesji!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Błędny indentyfikator sesji");
                    }
                }

                //operacja 011, odpowiedź na zaproszenie
                if (Convert.ToString((int)OperationType.InviteResponse, 2).PadLeft(3, '0') == operation)
                {
                    if (_sessionId == GetSessionIdFromData(data))
                    {
                        //operacja 011 odpowiedź 010, drugi klient przyjął zaproszenie
                        if (Convert.ToString((int)InviteResponseEnum.Accepted, 2).PadLeft(3, '0') == response)
                        {
                            //włączanie i wyłączanie odpowiednich okien i przycisków
                            _ = Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background, new Action(() =>
                            {
                                Content.Text += "Gość przyjął zaproszenie\n";
                                ChatBox.IsEnabled = true;
                                SendMessageButton.IsEnabled = true;
                                DisconnectButton.IsEnabled = true;
                                SendInviteButton.IsEnabled = false;
                                _connected = true;
                            }));

                        }

                        // operacja 011 odpowiedź 011, drugi klient odrzucił zaproszenie
                        else if (Convert.ToString((int)InviteResponseEnum.Denied, 2).PadLeft(3, '0') == response)
                        {

                            _ = Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background, new Action(() =>
                            {
                                Content.Text += "Gość odrzucił zaproszenie\n";
                            }));
                        }
                    }
                    else
                    {
                        MessageBox.Show("Błędny indentyfikator sesji");
                    }
                }

                // operacja 100, wysyłanie wiadomości 
                if (Convert.ToString((int)OperationType.Message, 2).PadLeft(3, '0') == operation)
                {
                    if (_sessionId == GetSessionIdFromData(data))
                    {
                        if (_connected)
                        {
                            //operacja 100 odpowiedź 001, potwierdzenie wysłania wiadomości
                            if (Convert.ToString((int)MessageEnum.Sent, 2).PadLeft(3, '0') == response)
                            {
                                //wyświetlenie wysłanej wiadomości
                                _ = Application.Current.Dispatcher.BeginInvoke(
                                DispatcherPriority.Background, new Action(() =>
                                {
                                    Content.Text += "Ty: " + GetMessageFromData(ref data) + "\n";
                                }));
                            }
                            //operacja 100 odpowiedź 000, otrzymano wiadomość
                            else if (Convert.ToString((int)MessageEnum.Send, 2).PadLeft(3, '0') == response)
                            {
                                //wyświetlenie odebranej wiadomości
                                _ = Application.Current.Dispatcher.BeginInvoke(
                                DispatcherPriority.Background, new Action(() =>
                                {
                                    Content.Text += "Gość: " + GetMessageFromData(ref data) + "\n";
                                }));
                            }
                            //operacja 100 odpowiedź 010, error
                            else if (Convert.ToString((int)MessageEnum.Error, 2).PadLeft(3, '0') == response)
                            {
                                _ = Application.Current.Dispatcher.BeginInvoke(
                                DispatcherPriority.Background, new Action(() =>
                                {
                                    Content.Text += "Błąd w sesji \n";
                                }));
                            }
                        }
                        else
                        {
                            MessageBox.Show("Nie jesteś z nikim połączony");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Błędny indentyfikator sesji");
                    }
                }

                //operacja 101, rozłączanie
                if (Convert.ToString((int)OperationType.Disconnect, 2).PadLeft(3, '0') == operation)
                {
                   // MessageBox.Show("pre connect checker");
                    if (_connected)
                    {
                        //MessageBox.Show("post connect checker " + _sessionId);
                        if (_sessionId == GetSessionIdFromData(data))
                        {
                            //MessageBox.Show("get session after checker");

                            //włączanie i wyłączanie odpowiednich pól po rozłączeniu
                            _ = Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background, new Action(() =>
                            {
                                _connected = false;
                                _sessionId = 0;
                                ChatBox.IsEnabled = false;
                                SendMessageButton.IsEnabled = false;
                                DisconnectButton.IsEnabled = false;
                                SendInviteButton.IsEnabled = false;
                                ServerIP.IsEnabled = true;
                                ServerPort.IsEnabled = true;
                                ConnectButton.IsEnabled = true;
                                //operacja 101 odpowiedź 001, pomyślnie rozłączono
                                if (Convert.ToString((int)DisconnectEnum.Success, 2).PadLeft(3, '0') == response)
                                    Content.Text += "Pomyślnie rozłączono \n";
                                //operacja 101 odpowiedź 010, drugi klient rozłączył
                                else
                                    Content.Text += "Gość się rozłączył \n";
                            }));
                        }
                        else
                        {
                            MessageBox.Show("Błędny indentyfikator sesji");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Nie jesteś połączony");
                    }

                }
                #endregion
            }
            else
            {
                MessageBox.Show("Otrzymano pakiet bez potwierdzenia otrzymania poprzedniego. Rozłączanie");
                Disconnect();
            }
            //MessageBox.Show("hello " + operation.ToString() + " " + response.ToString());
        }
        #region akcje na przyciskach

        //przycisk connect, połączenie z serwerem
        private async void SendRequest_ClickAsync(object sender, RoutedEventArgs e)
        {
            //wyrażenia regularne do IP i portu serwera
            Regex rx = new Regex(@"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
            Regex rx2 = new Regex("^[0-9]+$");

            if (rx.IsMatch(ServerIP.Text) && rx2.IsMatch(ServerPort.Text))
            {
                _serverPoint = new IPEndPoint(IPAddress.Parse(ServerIP.Text), Convert.ToInt32(ServerPort.Text));
                //wysłanie żądania id sesji 001 000
                Send(OperationType.GetSession, (int)SessionEnum.Request, 0);
                
#if DEBUG
#else
                //wyłaczanie przycisku na 5 sekund
                labelCounter(SendRequestTimer);
                ConnectButton.IsEnabled = false;
                await Task.Delay(5000);
                if (_sessionId == 0) ConnectButton.IsEnabled = true;
#endif

            }
            else
            {
                MessageBox.Show("Wpisz poprawne dane");
            }

        }

        //przycisk invite, zapraszanie drugiego klienta
        private void sendInviteButton_Click(object sender, RoutedEventArgs e)
        {
            if (_serverPoint != null && _sessionId != 0)
            {
                _ = Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background, new Action(async () =>
                {
                    //wysłanie zaproszenia, 010 000
                    Send(OperationType.Invite, (int)InviteEnum.Send, _sessionId);
                    
                    
#if DEBUG

#else
                    //wyłączanie przycisku na 5 sekund
                    SendInviteButton.IsEnabled = false;
                    labelCounter(SendInviteCounter);
                    await Task.Delay(5000);
                    if (!_connected) SendInviteButton.IsEnabled = true;
#endif

                }));


            }
            else
            {
                MessageBox.Show("Najpierw zainicjuj połączenie");
            }

        }

        //przycisk send, wysyłanie wiadomości
        private void SendMessageButton_Click(object sender, RoutedEventArgs e)
        {
            if (_serverPoint != null && _sessionId != 0 && _connected)
            {
                //wysyłanie wiadomości, 100 000
                SendMessage(OperationType.Message, (int)MessageEnum.Send, ChatBox.Text, _sessionId);
                
                ChatBox.Clear();
                ChatBox.Focus();
            }
            else
            {
                MessageBox.Show("Najpierw zainicjuj połączenie");
            }
        }

        //przycisk disconnect, rozłączanie z serwerem
        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            Disconnect();
        }

        //aktywowanie przycisku wysyłania wiadomości
        private void ChatBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) SendMessageButton_Click(sender, e);
        }


        //wysyłanie komunikatu rozłączającego przy zamykaniu aplikacji
        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if(_connected) Disconnect();
        }

        #endregion

        #region przetwarzanie datagramu
        public void Send(OperationType operation, int response, int ssid)
        {
            //budowanie pakietu
            StringBuilder sb = new StringBuilder();
            //pola operacji i odpowiedzi
            sb.Append(Convert.ToString((int)operation, 2).PadLeft(3, '0'));
            sb.Append(Convert.ToString(response, 2).PadLeft(3, '0'));
            //pole długości danych (0 w przypadku komunikatu innego niż wiadomość)
            for (int i = 0; i < 32; i++)
            {
                sb.Append("0");
            }
            //id sesji
            var sessionIdArr = Convert.ToByte(ssid);
            sb.Append(Convert.ToString(sessionIdArr, 2).PadLeft(8, '0'));
            //dopełnienie do pełnego bajtu
            sb.Append("00");
            string binaryString = sb.ToString();
            int bytesNumber = sb.Length / 8;
            byte[] dg = new byte[bytesNumber];
            for (int i = 0; i < bytesNumber; i++)
            {
                dg[i] = Convert.ToByte(binaryString.Substring(i * 8, 8), 2);
            }
            using (UdpClient sender = new UdpClient(1300))
            {
                //wysyłanie pakietu i ustawianie oczekiwania na ACK
                sender.Send(dg, dg.Length, _serverPoint);
                if (operation != OperationType.Ack) _waitForAck = true;
            }
            
#if DEBUG
            _ = Application.Current.Dispatcher.BeginInvoke(
                DispatcherPriority.Background, new Action(() =>
                {
                    Content.Text += "sent: " + Convert.ToString(dg[0], 2).PadLeft(8, '0') + " " + _waitForAck + "\n";
                }));
#endif


        }
        //wysyłanie wiadomości
        public void SendMessage(OperationType operation, int response, string message, int ssid)
        {
            //budowanie pakietu
            StringBuilder sb = new StringBuilder();
            //pola operacji i odpowiedzi
            if (operation != OperationType.Message) throw new Exception("Zły typ operacji");
            sb.Append(Convert.ToString((int)operation, 2).PadLeft(3, '0'));
            sb.Append(Convert.ToString(response, 2).PadLeft(3, '0'));
            //pole długości danych - długość wiadomości
            sb.Append(Convert.ToString(message.Length, 2).PadLeft(32, '0'));
            //wiadomość
            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            foreach (byte b in messageBytes)
            {
                sb.Append(Convert.ToString(b, 2).PadLeft(8, '0'));
            }
            //id sesji
            var sessionIdArr = Convert.ToByte(ssid);
            sb.Append(Convert.ToString(sessionIdArr, 2).PadLeft(8, '0'));
            //dopełnienie do pełnego bajtu
            sb.Append("00");
            string binaryString = sb.ToString();
            int bytesNumber = sb.Length / 8;
            byte[] dg = new byte[bytesNumber];
            for (int i = 0; i < bytesNumber; i++)
            {
                dg[i] = Convert.ToByte(binaryString.Substring(i * 8, 8), 2);
            }
            using (UdpClient sender = new UdpClient(1300))
            {
                //wysłanie wiadomości i ustawianie oczekiwania na ACK
                sender.Send(dg, dg.Length, _serverPoint);
                if (operation != OperationType.Ack) _waitForAck = true;
            }

        }

        //odczytywanie pola id sesji
        public int GetSessionIdFromData(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            int messageLength = GetMessageLengthFromData(ref data);
            sb.Append(Convert.ToString(data[4 + messageLength], 2).PadLeft(8, '0').Substring(6, 2));
            sb.Append(Convert.ToString(data[5 + messageLength], 2).PadLeft(8, '0').Substring(0, 6));
            return Convert.ToInt32(sb.ToString(), 2);
        }

        //odczytywanie pola długości danych
        public int GetMessageLengthFromData(ref byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Convert.ToString(data[0], 2).PadLeft(8, '0').Substring(6, 2));
            for (int i = 1; i < 4; i++) sb.Append(Convert.ToString(data[i], 2).PadLeft(8, '0'));
            sb.Append(Convert.ToString(data[4], 2).PadLeft(8, '0').Substring(0, 6));
            return Convert.ToInt32(sb.ToString(), 2);
        }

        //odczytywanie wiadomości z danych
        public string GetMessageFromData(ref byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            int bytesToRead = GetMessageLengthFromData(ref data);   //odczytywanie długości wiadomości
            for (int i = 4; i < 4 + bytesToRead; i++)
            {
                //bajty danych przesunięte o 2 bity, więc najpierw odczytujemy 2 ostatnie bity pierwszego bajtu
                //a później 6 bitów drugiego
                sb.Append(Convert.ToString(data[i], 2).PadLeft(8, '0').Substring(6, 2));
                sb.Append(Convert.ToString(data[i + 1], 2).PadLeft(8, '0').Substring(0, 6));
            }
            string messageBits = sb.ToString();
            List<byte> bytesList = new List<byte>();
            for (int i = 0; i < bytesToRead; i++)
            {
                bytesList.Add(Convert.ToByte(messageBits.Substring(8 * i, 8), 2));
            }
            string message = Encoding.ASCII.GetString(bytesList.ToArray());
            Console.WriteLine(message);
            return message;
        }
        #endregion

        //rozłączanie z serwerem
        private void Disconnect()
        {
            if (_serverPoint != null && _sessionId != 0)
            {
                //MessageBox.Show("Before packet sent");

                //wysłanie pakietu rozłączającego, 101 000
                Send(OperationType.Disconnect, (int)DisconnectEnum.Request, _sessionId);
                
                //MessageBox.Show("After packet sent");
            }
            
        }

        //odliczanie przy chwilowo wyłączanym przycisku
        private async void labelCounter(Label l)
        {
            
#if DEBUG
#else
            for (int i = 4; i >= 0; i--)
            {
                l.Content = "(" + i.ToString() + ")";
                await Task.Delay(1000);
            }
            l.Content = "";
#endif


        }
    }
}
